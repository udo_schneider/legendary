import Config


defmodule App.RuntimeConfig.Test do
  def secret_key_base, do: System.get_env("SECRET_KEY_BASE", "r2eN53mJ9RmlGz9ZQ7xf43P3Or59aaO9rdf5D3hRcsuiH44pGW9kPGfl5mt9N1Gi")
  def live_view_signing_salt, do: System.get_env("LIVE_VIEW_SIGNING_SALT", "g5ltUbnQ")
  def url_host, do: "localhost"
  def default_url_port, do: "4000"
  def default_listen_port, do: "4000"
  def endpoint_extra_opts(_), do: []

  def ecto_pool(), do: Ecto.Adapters.SQL.Sandbox
  def db_config() do
    [
      username: "postgres",
      password: "postgres",
      database: "legendary_test#{System.get_env("MIX_TEST_PARTITION")}",
      hostname: System.get_env("DATABASE_URL") || "localhost",
    ]
  end

  def mailer_config(), do: []

  def cluster_topologies(), do: []

  def object_storage_scheme(), do: "http://"
  def object_storage_host(), do: "localhost"
  def object_storage_port(), do: App.RuntimeConfig.url_port()
  def object_storage_bucket(), do: "uploads"
  def object_storage_access_key_id(), do: "test-access-key-id"
  def object_storage_secret_access_key(), do: "test-secret-access-key"
end

config :core, Legendary.CoreMailer, adapter: Bamboo.TestAdapter

config :content, Oban, crontab: false, queues: false, plugins: false

config :logger, level: :warn

config :waffle,
  storage: Waffle.Storage.Local,
  storage_dir_prefix: "priv/test/static/"

config :object_storage, :signature_generator, Legendary.ObjectStorageWeb.CheckSignatures.MockSignatureGenerator
