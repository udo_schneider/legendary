defmodule Legendary.Content do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use Legendary.Content, :controller
      use Legendary.Content, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: Legendary.Content

      import Plug.Conn
      import Legendary.Content.Gettext
      alias Legendary.Content.Router.Helpers, as: Routes
    end
  end

  def live_view do
    quote do
      use Phoenix.LiveView,
        layout: {AppWeb.LayoutView, "live.html"}

      import Legendary.Content.LiveHelpers

      unquote(view_helpers())
    end
  end

  def live_component do
    quote do
      use Phoenix.LiveComponent

      import Legendary.Content.LiveHelpers

      unquote(view_helpers())
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/content_web/templates",
        namespace: Legendary.Content,
        pattern: "**/*"

        # Import convenience functions from controllers
        import Phoenix.Controller,
        only: [get_flash: 1, get_flash: 2, view_module: 1, view_template: 1]

        import Phoenix.LiveView.Helpers

        import ShorterMaps

      # Include shared imports, aliases and functions for views
      unquote(view_helpers())
    end
  end

  def router do
    quote do
      use Phoenix.Router

      import Plug.Conn
      import Phoenix.Controller
      import Phoenix.LiveView.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import Legendary.Content.Gettext
    end
  end

  defp view_helpers do
    quote do
       # Use all HTML functionality (forms, tags, etc)
       use Phoenix.HTML

       # Import basic rendering functionality (render, render_layout, etc)
       import Phoenix.View

       # Make shared fonctions available in live_views, live_components and views
       def gravatar_url_for_email(email) do
        email
        |> Kernel.||("noreply@example.com")
        |> String.trim()
        |> String.downcase()
        |> (&(:crypto.hash(:md5, &1))).()
        |> Base.encode16()
        |> String.downcase()
        |> (&("https://www.gravatar.com/avatar/#{&1}")).()
      end

      def process_content(text) do
        text
        |> Earmark.as_html!()
      end

       import Legendary.Content.ErrorHelpers
       import Legendary.Content.Gettext
       import Legendary.CoreWeb.Helpers
       import Phoenix.LiveView.Helpers
       alias Legendary.Content.Router.Helpers, as: Routes
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
