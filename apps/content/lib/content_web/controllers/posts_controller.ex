defmodule Legendary.Content.PostsController do
  use Legendary.Content, :controller

  import Phoenix.LiveView.Controller, only: [live_render: 2, live_render: 3]

  alias Legendary.Content.{Options, Posts}
  alias Legendary.Content.PostPageLive
  alias Legendary.Content.PostsPageLive
  alias Legendary.Content.Posts.Static

  plug :put_layout, false when action in [:preview]

  def preview(conn, %{"post" => post_params}) do
    post = Posts.preview_post(post_params)

    conn
    |> render("show.html", post: post, page: 1, thumbs: [])
  end
end
