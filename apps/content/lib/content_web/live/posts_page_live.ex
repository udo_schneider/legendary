defmodule Legendary.Content.PostsPageLive do
  @moduledoc """
  LiveView for a list of posts.
  """
  use Legendary.Content, :live_view

  alias Legendary.Content.Posts
  alias Legendary.Content.PostComponent

  def mount(_params, session, socket) do
    socket = assign_defaults(socket, session)
    ip_address = get_ip_address(socket)
    {:ok, assign(socket, ip_address: ip_address)}
  end

  def handle_params(params, _url, socket) do
    page = params |> Map.get("page", "1")
    params = params |> Map.merge(%{"page" => page})
    category = params |> Map.get("category")
    posts =  Posts.list_posts(params)
    thumbs = posts |> Posts.thumbs_for_posts()
    last_page = Posts.last_page(params)

    socket =
      assign(socket,
        posts: posts,
        thumbs: thumbs,
        page: String.to_integer(page),
        last_page: last_page,
        category: category
      )

    {:noreply, socket}
  end

  def render(assigns) do
    ~H"""
    <%= for post <- @posts do %>
      <.live_component module={PostComponent}
                       id={"post-list-#{post_element_suffix(post)}"}
                       post={post}
                       thumbs={@thumbs}
                       page={@page}
                       single={false}
                       ip_address={@ip_address} />
    <% end %>

    <nav class="max-w-xl mx-auto flex justify-center">
      <div class="flex shadow rounded">
        <%= paginator(1..@last_page, @page, fn first..last, page ->
          live_patch page,
                     to: paginated_posts_live_path(@socket, @category, page),
                     style: "min-width: 3.5rem",
                     class: "#{if page == @page, do: "font-bold"} text-center flex-1 border bg-gray-100 hover:bg-gray-300 text-gray-500 p-4 #{group_rounding_class(first..last, page)}"
        end)
        %>
      </div>
    </nav>

    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/highlight.min.js"></script>
    <script charset="UTF-8"
      src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/languages/elixir.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    """
  end

  defp paginated_posts_live_path(socket, category, page) do
    case category do
      nil ->
        Routes.blog_page_path(socket, __MODULE__, page)
      _ ->
        Routes.category_page_path(socket, __MODULE__, category, page)
    end
  end
end
