defmodule Legendary.Content.PaginationComponent do
  @moduledoc """
  Live Component for pagination controls.
  """

  use Legendary.Content, :live_component

  alias Legendary.Content.PostsPageLive

  def render(assigns) do
    ~H"""
    <div>
      <%= if Legendary.Content.Post.paginated_post?(@post) do %>
        <nav class="paginator">
          Page:
            <% pagination_link(@post, @socket, assigns) %>
        </nav>
      <% end %>
    </div>
    """
  end

  defp pagination_link(post, socket, assigns) do
    Enum.map(
      1..Legendary.Content.Post.content_page_count(post),
      fn page ->
        if assigns[:current_page] == nil || assigns[:current_page] != page do
          live_patch page, to: Routes.live_path(socket, PostsPageLive, id: "#{post.id}", post: post, page: page)
        else
          content_tag :span, page, class: "paginator-page"
        end
    end)
  end
end
