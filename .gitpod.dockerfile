FROM gitpod/workspace-postgres

USER root

ENV DEBIAN_FRONTEND noninteractive
ENV PATH="$HOME/.asdf/bin:$HOME/.asdf/shims:$PATH"

# Uninstall Homrbrew which comes with gitpod/workspace-postgres so the Legendary scrip won't try to install the dependencies again using brew
RUN /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/uninstall.sh)" \
    && rm -rf /home/linuxbrew

# Install asdf, asdf-erlang and asdf-nodejs dependencies.
RUN apt-get update \
    && apt-get -y install curl git \
    && apt-get -y install build-essential autoconf m4 libncurses5-dev libwxgtk3.0-gtk3-dev libgl1-mesa-dev libglu1-mesa-dev libpng-dev libssh-dev unixodbc-dev xsltproc fop libxml2-utils libncurses-dev openjdk-11-jdk \
    && apt-get -y install dirmngr gpg curl gawk

# Install inotify-tools for Phoenix Live Reloading support
# @see https://hexdocs.pm/phoenix/installation.html#inotify-tools-for-linux-users
RUN apt-get -y install inotify-tools

# Install asdf
RUN git clone https://github.com/asdf-vm/asdf.git "$HOME/.asdf" --branch v0.8.1

# Add asdf erlang, elixir and node plugins
RUN asdf plugin-add erlang \
    && asdf plugin-add elixir \
    && asdf plugin-add nodejs \
    && $HOME/.asdf/plugins/nodejs/bin/import-release-team-keyring

# Versions of erlang, elixir and node are pulled from .tool-versions at the root of the Legendary project directory
COPY .tool-versions $HOME/

# Install erlang, elixir and node and remove left over files
RUN asdf install \
    && rm -rf  /tmp/* \
    && rm $HOME/.tool-versions

# Allow gitpod group to edit .asdf directory
RUN true \
	&& chown -R root:gitpod $HOME/.asdf \
    && chmod -R g+rw $HOME/.asdf
